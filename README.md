# Portfolio

Code used for my portfolio.


## Install

This projects requires Node 10 and yarn.

``` bash
# Clone project
git clone https://framagit.org/alcalyn/portfolio.git
cd portfolio/

# This can be done by running docker and bash into it:

# Install dependencies
yarn

# Build assets
yarn build
```

Then open `index.html` in browser, with a local webserver.

Run `yarn watch` while develop to automatically rebuild assets.


## License

![cc-by-4.0](cc-by.png)
[Boostraptheme](https://boostraptheme.com)

This work is under [cc-by-4.0 license](LICENSE), and based on this theme: <https://boostraptheme.com/free-theme/26/personal-portfolio>.
