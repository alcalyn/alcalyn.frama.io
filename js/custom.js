(function ($) {
    'use strict'; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*=\'#\']:not([href=\'#\'])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top)
                }, 1000, 'easeInOutExpo');
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#sideNav'
    });


    $(function () {
        loadDeferedImages();
        enableFilter('portfolio');
        enableFilter('timeline');
        counterUp();
        enableEventsTracking();
        initContactForm();
    });

    function enableFilter(type) {
        $('.filter-'+type).click(function () {
            var value = $(this).attr('data-filter');
            if (value == 'all') {
                $('.filter-item-'+type).show('1000');
            }
            else {
                $('.filter-item-'+type).not('.' + value).hide('3000');
                $('.filter-item-'+type).filter('.' + value).show('3000');
            }

            $('.filter-'+type).removeClass('active');
            $(this).addClass('active');
        });

        $('[data-filter="all"].filter-'+type).addClass('active');
    }

    function counterUp() {
        $('.counter').counterUp({
            delay: 10,
            time: 2000
        });
    }

    function loadDeferedImages() {
        const imgDefer = document.getElementsByTagName('img');

        for (var i = 0; i < imgDefer.length; i++) {
            if (imgDefer[i].getAttribute('data-src')) {
                imgDefer[i].setAttribute('src', imgDefer[i].getAttribute('data-src'));
            }
        }
    }

    function initContactForm() {
        $('form.contact-form').submit(function (e) {
            e.preventDefault();

            const $form = $(this);
            const $submitButton = $form.find('[type=submit]').attr('disabled', 'disabled');
            const formData = {};
            $form.serializeArray().forEach(({name, value}) => {
                formData[name] = value;
            });

            console.log(formData);

            $submitButton.prop('disabled', true);

            $.ajax({
                url: $form.attr('action'),
                method: $form.attr('method'),
                data: {
                    from: formData['email'],
                    subject: formData['subject'],
                    text: [
                        'Message depuis le portfolio',
                        '',
                        'Nom complet:',
                        formData['full-name'],
                        '',
                        'Message:',
                        formData['message'],
                    ].join('\n'),
                },
            }).done(() => {
                $submitButton.hide();
                $('.sent-failed').addClass('d-none');
                $('.sent-confirm').removeClass('d-none');
                $form.trigger('reset');
            }).fail(() => {
                $submitButton.prop('disabled', false);
                $('.sent-failed').removeClass('d-none');
            });

            return false;
        });
    }

    function enableEventsTracking() {
        // Nav links
        $('#sideNav .nav-link').click(function () {
            const projectName = $(this).attr('href').substr(1);
            track('menu', 'goto-' + projectName);
        });

        // Portfolio opens
        $('.portfolio-link').click(function () {
            const projectName = $(this).attr('href').substr(1);
            track('portfolio', 'open-' + projectName);
        });

        // Portfolio filters
        $('.filter-portfolio').click(function () {
            const value = $(this).attr('data-filter');
            track('portfolio', 'filter-' + value);
        });

        // Timeline filters
        $('.filter-timeline').click(function () {
            const value = $(this).attr('data-filter');
            track('timeline', 'filter-' + value);
        });
    }

    /**
     * @param {String} category Section of the portfolio, "portfolio", "timeline", ...
     * @param {String} action Action done in this section, "filter-pro", "open-navitia", ...
     * @param {String} name unused
     * @param {String} value unused
     */
    function track(category, action, name, value) {
        const _paq = window._paq || [];

        _paq.push(['trackEvent', category, action, name, value]);
    }
})(jQuery);
